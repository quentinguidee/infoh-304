#ifndef SORTING_H
#define SORTING_H

#include <string>
#include <vector>

using namespace std;

typedef vector<string> list;

void merge(list &l, int left, int mid, int right);
void merge_sort(list &l, int left, int right);
void insertion_sort(vector<string> &l, int left, int right);
int check_sorted(vector<string> &l1, vector<string> &l2);
vector<unsigned long long> compare_algorithms(list &l_merge, list &l_stl, list &l_insertion);

#endif /* SORTING_H */
