#include <chrono>
#include <cstddef>
#include <iostream>
#include <random>
#include <string>
#include <thread>
#include <unordered_set>

extern "C" {
#include <UbigraphAPI.h>
}

using namespace std;

class BinarySearchTree
{
private:
    struct node
    {
        node* left;
        node* right;
        node* parent;
        int content;
    };
    node* root;
    node* tree_search(int content);
    node* tree_search(int content, node* location);
    void delete_no_child(node* location);
    void delete_left_child(node* location);
    void delete_right_child(node* location);
    void delete_two_children(node* location);

public:
    BinarySearchTree()
    {
        root = NULL;
    }
    bool isEmpty()
    {
        return root == NULL;
    }
    void insert_element(int content);
    void delete_element(int content);
    void inorder(node* location);
    void print_inorder();
};

void BinarySearchTree::insert_element(int content)
{
    // New node
    node* n = new node();
    n->content = content;
    n->left = NULL;
    n->right = NULL;
    n->parent = NULL;

    // For visualization
    int eid, vid;
    this_thread::sleep_for(chrono::milliseconds(100));
    ubigraph_new_vertex_w_id(content);
    ubigraph_set_vertex_attribute(content, "color", "#0000ff");
    ubigraph_set_vertex_attribute(content, "label", to_string(content).c_str());

    if (isEmpty())
    {
        root = n;
        ubigraph_set_vertex_attribute(content, "color", "#ff0000");
        this_thread::sleep_for(chrono::milliseconds(100));
        ubigraph_set_vertex_attribute(content, "color", "#0000ff");
        this_thread::sleep_for(chrono::milliseconds(100));
    }
    else
    {
        node* pointer;
        pointer = root;

        while (pointer)
        {
            n->parent = pointer;

            if (n->content > pointer->content)
            {
                vid = pointer->content;
                ubigraph_set_vertex_attribute(vid, "color", "#ff0000");
                this_thread::sleep_for(chrono::milliseconds(100));
                ubigraph_set_vertex_attribute(vid, "color", "#0000ff");
                this_thread::sleep_for(chrono::milliseconds(100));

                pointer = pointer->right;
            }
            else
            {
                vid = pointer->content;
                ubigraph_set_vertex_attribute(vid, "color", "#ff0000");
                this_thread::sleep_for(chrono::milliseconds(100));
                ubigraph_set_vertex_attribute(vid, "color", "#0000ff");
                this_thread::sleep_for(chrono::milliseconds(100));

                pointer = pointer->left;
            }
        }

        if (n->content < n->parent->content)
        {
            n->parent->left = n;
            this_thread::sleep_for(chrono::milliseconds(200));
            eid = ubigraph_new_edge(n->parent->content, content);
            ubigraph_set_edge_attribute(eid, "oriented", "true");
        }
        else
        {
            n->parent->right = n;
            this_thread::sleep_for(chrono::milliseconds(200));
            eid = ubigraph_new_edge(n->parent->content, content);
            ubigraph_set_edge_attribute(eid, "oriented", "true");
        }
    }
}

void BinarySearchTree::inorder(node* location)
{
    if (location == NULL) return;

    inorder(location->left);
    cout << location->content << " ";
    inorder(location->right);
}

void BinarySearchTree::print_inorder()
{
    inorder(root);
}

BinarySearchTree::node* BinarySearchTree::tree_search(int content)
{
    return tree_search(content, root);
}

BinarySearchTree::node* BinarySearchTree::tree_search(int content, node* location)
{
    if (location == NULL) return NULL;

    if (location->content == content)
        return location;
    else if (content < location->content)
        return tree_search(content, location->left);
    else
        return tree_search(content, location->right);
}

void BinarySearchTree::delete_no_child(node* location)
{
    if (location == root)
    {
        root = NULL;
        ubigraph_remove_vertex(location->content);
        delete location;
        return;
    }

    if (location->parent->left == location)
        location->parent->left = NULL;
    else
        location->parent->right = NULL;

    ubigraph_set_vertex_attribute(location->content, "color", "#00ff00");
    this_thread::sleep_for(chrono::milliseconds(100));
    ubigraph_remove_vertex(location->content);
    delete location;
}

void BinarySearchTree::delete_left_child(node* location)
{
    if (location == root)
    {
        root = location->left;
        location->left->parent = NULL;
        ubigraph_remove_vertex(location->content);
        delete location;
        return;
    }

    if (location->parent->left == location)
        location->parent->left = location->left;
    else
        location->parent->right = location->left;

    location->left->parent = location->parent;

    ubigraph_set_vertex_attribute(location->content, "color", "#00ff00");
    this_thread::sleep_for(chrono::milliseconds(100));
    ubigraph_remove_vertex(location->content);
    int eid = ubigraph_new_edge(location->parent->content, location->left->content);
    ubigraph_set_edge_attribute(eid, "oriented", "true");
    delete location;
}

void BinarySearchTree::delete_right_child(node* location)
{
    if (location == root)
    {
        root = location->right;
        location->right->parent = NULL;
        ubigraph_remove_vertex(location->content);
        delete location;
        return;
    }

    if (location->parent->left == location)
        location->parent->left = location->right;
    else
        location->parent->right = location->right;

    location->right->parent = location->parent;

    ubigraph_set_vertex_attribute(location->content, "color", "#00ff00");
    this_thread::sleep_for(chrono::milliseconds(100));
    ubigraph_remove_vertex(location->content);
    int eid = ubigraph_new_edge(location->parent->content, location->right->content);
    ubigraph_set_edge_attribute(eid, "oriented", "true");
    delete location;
}

void BinarySearchTree::delete_two_children(node* location)
{
    node* next = location->right;
    node* left_child = location->left;
    node* new_child;

    while ((left_child = left_child->left) != NULL)
        ;

    int new_content = next->content;

    if (next->right == NULL)
        delete_no_child(next);
    else
        delete_right_child(next);

    location->content = new_content;

    ubigraph_remove_vertex(location->content);
    ubigraph_new_vertex_w_id(new_content);
    ubigraph_set_vertex_attribute(new_content, "color", "#0000ff");
    ubigraph_set_vertex_attribute(new_content, "label", to_string(new_content).c_str());

    int eid;
    if (location->parent != NULL)
        eid = ubigraph_new_edge(location->parent->content, new_content);
    if (location->left != NULL)
        eid = ubigraph_new_edge(location->left->content, new_content);
    if (location->right != NULL)
        eid = ubigraph_new_edge(location->right->content, new_content);

    ubigraph_set_edge_attribute(eid, "oriented", "true");
}

void BinarySearchTree::delete_element(int content)
{
    node* location = tree_search(content);
    if (location == NULL) return;

    if (location->left == NULL && location->right == NULL)
        delete_no_child(location);
    else if (location->left == NULL && location->right != NULL)
        delete_right_child(location);
    else if (location->left != NULL && location->right == NULL)
        delete_left_child(location);
    else
        delete_two_children(location);

    this_thread::sleep_for(chrono::milliseconds(300));
}

int main()
{
    ubigraph_clear();
    unordered_set<int> vertices;
    int vertex;
    BinarySearchTree bst;

    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<int> r(0, 1000);

    /// RANDOM
    // for (int i = 1; i <= 10; i++)
    // {
    //     vertex = r(gen);
    //     if (vertices.count(vertex) == 0)
    //     {
    //         vertices.insert(vertex);
    //         bst.insert_element(vertex);
    //     }
    //     else
    //         i--;
    // }

    /// DÉSÉQUILIBRÉ
    // for (int i = 1; i <= 10; i++)
    // {
    //     vertex = i;
    //     if (vertices.count(vertex) == 0)
    //     {
    //         vertices.insert(vertex);
    //         bst.insert_element(vertex);
    //     }
    //     else
    //         i--;
    // }

    /// ÉQUILIBRÉ
    int elements[] = {7, 9, 4, 10, 8, 5, 2, 1, 3, 6};
    for (int i = 0; i < 10; i++)
    {
        vertex = elements[i];
        if (vertices.count(vertex) == 0)
        {
            vertices.insert(vertex);
            bst.insert_element(vertex);
        }
        else
            i--;
    }

    bst.print_inorder();

    for (int i = 1; i <= 10; i++)
        bst.delete_element(i);

    return 0;
}
