#include <chrono>
#include <iostream>
#include <random>
#include <string>
#include <thread>
#include <unordered_set>
#include <vector>

using namespace std;

class BinarySearchTree
{
private:
    struct node
    {
        node* left;
        node* right;
        node* parent;
        int content;
    };
    node* root;
    node* tree_search(int content);
    node* tree_search(int content, node* location);
    void delete_no_child(node* location);
    void delete_left_child(node* location);
    void delete_right_child(node* location);
    void delete_two_children(node* location);

public:
    BinarySearchTree()
    {
        root = NULL;
    }
    bool isEmpty()
    {
        return root == NULL;
    }
    void insert_element(int content);
    void delete_element(int content);
    void inorder(node* location);
    void print_inorder();
    void print_tree();
    pair<int, int> create_tree_lines(node* start, vector<string>& value_lines, vector<string>& split_lines, vector<string>& drop_lines, int depth = 0, int previous_width = 0);
};

void BinarySearchTree::insert_element(int content)
{
    // New node
    node* n = new node();
    n->content = content;
    n->left = NULL;
    n->right = NULL;
    n->parent = NULL;

    if (isEmpty())
    {
        root = n;
    }
    else
    {
        node* pointer;
        pointer = root;

        while (pointer)
        {
            n->parent = pointer;

            if (n->content > pointer->content)
                pointer = pointer->right;
            else
                pointer = pointer->left;
        }

        if (n->content < n->parent->content)
            n->parent->left = n;
        else
            n->parent->right = n;
    }
}

void BinarySearchTree::inorder(node* location)
{
}

void BinarySearchTree::print_inorder()
{
    inorder(root);
}

void BinarySearchTree::print_tree()
{
    vector<string> value_lines;
    vector<string> split_lines;
    vector<string> drop_lines;
    create_tree_lines(root, value_lines, split_lines, drop_lines);
    for (int i = 0; i < value_lines.size(); i++)
    {
        if (i > 0)
            cout << drop_lines[i] << endl;
        cout << value_lines[i] << endl;
        if (i < value_lines.size() - 1)
            cout << split_lines[i] << endl;
    }
}

pair<int, int> BinarySearchTree::create_tree_lines(node* start, vector<string>& value_lines, vector<string>& split_lines, vector<string>& drop_lines, int depth, int previous_width)
{
    if (start == NULL)
        return {0, -1};

    if (value_lines.size() <= depth)
    {
        value_lines.push_back("");
        split_lines.push_back("");
        drop_lines.push_back("");
    }

    if (value_lines[depth].size() < previous_width)
    {
        for (int i = value_lines[depth].size(); i < previous_width; i++)
        {
            value_lines[depth] += " ";
            split_lines[depth] += " ";
            drop_lines[depth] += " ";
        }
    }

    pair<int, int> retleft = create_tree_lines(start->left, value_lines, split_lines, drop_lines, depth + 1, previous_width);
    int width_of_left_sub_tree = retleft.first;
    int center_left = retleft.second;

    int real_left_width = width_of_left_sub_tree + 1;
    if (width_of_left_sub_tree == 0)
        real_left_width = 0;
    int new_previous_width = previous_width + real_left_width;

    pair<int, int> retright = create_tree_lines(start->right, value_lines, split_lines, drop_lines, depth + 1, new_previous_width);
    int width_of_right_sub_tree = retright.first;
    int center_right = retright.second;

    int center = (center_right - center_left + real_left_width) / 2 + center_left;
    if (center_left == -1)
        center = center_right;
    else if (center_right == -1)
        center = center_left;

    string value = to_string(start->content);
    int value_length = value.size();
    int total_width = width_of_left_sub_tree + width_of_right_sub_tree;
    if (width_of_left_sub_tree != 0 && width_of_right_sub_tree != 0)
        total_width += 1;
    if (total_width < value_length)
    {
        total_width = value_length;
        center = total_width / 2;
    }

    int begin_of_value = center - (value_length) / 2;

    for (int i = 0; i < total_width; i++)
    {
        if (i >= begin_of_value && i < begin_of_value + value_length)
        {
            value_lines[depth] += value[i - begin_of_value];
        }
        else
        {
            value_lines[depth] += " ";
        }

        if (i == center)
            drop_lines[depth] += "|";
        else
            drop_lines[depth] += " ";

        if (i == center)
        {
            if (start->left == NULL && start->right == NULL)
                split_lines[depth] += " ";
            else
                split_lines[depth] += "|";
        }
        else if (i > center_left && i < center)
        {
            if (start->left != NULL)
            {
                split_lines[depth] += "_";
            }
            else
            {
                split_lines[depth] += " ";
            }
        }
        else if (i > center && i < center_right + real_left_width)
        {
            if (start->right != NULL)
            {
                split_lines[depth] += "_";
            }
            else
            {
                split_lines[depth] += " ";
            }
        }
        else
        {
            split_lines[depth] += " ";
        }
    }
    value_lines[depth] += " ";
    drop_lines[depth] += " ";
    split_lines[depth] += " ";

    return {total_width, center};
}

BinarySearchTree::node* BinarySearchTree::tree_search(int content)
{
    return tree_search(content, root);
}

BinarySearchTree::node* BinarySearchTree::tree_search(int content, node* location)
{
    return NULL;
}

void BinarySearchTree::delete_no_child(node* location)
{
}

void BinarySearchTree::delete_left_child(node* location)
{
}

void BinarySearchTree::delete_right_child(node* location)
{
}

void BinarySearchTree::delete_two_children(node* location)
{
}

void BinarySearchTree::delete_element(int content)
{
}

int main()
{
    unordered_set<int> vertices;
    int vertex;
    BinarySearchTree bst;

    random_device rd;
    mt19937 gen(rd());
    uniform_int_distribution<int> r(0, 1000);

    for (int i = 1; i <= 10; i++)
    {
        vertex = r(gen);
        if (vertices.count(vertex) == 0)
        {
            vertices.insert(vertex);
            bst.insert_element(vertex);
        }
        else
            i--;
    }

    bst.print_inorder();
    bst.print_tree();

    return 0;
}
