#include <fstream>
#include <iostream>
#include <string>

#include "list.h"

using std::cout;
using std::endl;
using std::ifstream;
using std::string;

int main(int argc, char* argv[])
{
    if (argc != 2)
        return 1;

    string filename = argv[1];
    ifstream file(filename);

    if (!file.is_open())
    {
        cout << "Impossible d'ouvrir ce fichier" << endl;
        return 2;
    }

    List list;
    string word;

    while (file >> word)
        list.insert(word);

    file.close();

    list.print();

    return 0;
}
