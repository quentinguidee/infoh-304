#ifndef NODE_H
#define NODE_H

#include <iostream>
#include <string>

using std::string;

class Node
{
private:
    string data;
    Node* next;

public:
    Node();
    Node(string text);

    void setNext(Node* n);
    Node* getNext() const;
    string getData() const;
};

#endif /* NODE_H */
