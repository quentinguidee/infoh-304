#include "node.h"

Node::Node() :
    data(""),
    next(nullptr)
{
}

Node::Node(string text) :
    data(text),
    next(nullptr)
{
}

void Node::setNext(Node* n)
{
    next = n;
}

Node* Node::getNext() const
{
    return next;
}

string Node::getData() const
{
    return data;
}
