#ifndef LIST_H
#define LIST_H

#include <string>

#include "node.h"

class List
{
private:
    int nodesCount;
    Node* head;

public:
    List();
    ~List();

    void insert(string text);
    void pop();
    void print() const;

    int getNodesCount() const;
    Node* getHead() const;
};

#endif /* LIST_H */
