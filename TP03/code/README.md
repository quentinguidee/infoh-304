# Séance 3

## Compilation

Tout :

```bash
make
```

Exercice 1 et 2 :

```bash
make ex2
```

Exercice 3 :

```bash
make ex3
```

## Run

```bash
./ex2
./ex3
```
