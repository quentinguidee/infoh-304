#include "list.h"

int main()
{
    List list;

    list.insert("Node A");
    list.insert("Node B");
    list.insert("Node C");

    list.print();

    list.pop();

    list.print();
}
