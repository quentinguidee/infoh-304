#include "list.h"

using std::cout;
using std::endl;

List::List() :
    nodesCount(0), head(nullptr) {}

List::~List()
{
    while (nodesCount != 0)
        pop();
}

void List::insert(string text)
{
    Node* node = new Node(text);
    node->setNext(head);
    nodesCount++;
    head = node;
}

void List::pop()
{
    if (head == nullptr) return;

    Node* currentHead = head;
    head = head->getNext();
    nodesCount--;
    delete currentHead;
}

void List::print() const
{
    Node* next = head;
    while (next != nullptr)
    {
        cout << next->getData() << endl;
        next = next->getNext();
    }
}

int List::getNodesCount() const
{
    return nodesCount;
}

Node* List::getHead() const
{
    return head;
}
