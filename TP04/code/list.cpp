#include "list.h"

#include <ostream>

using std::cout;
using std::endl;
using std::ostream;

List::List() :
    nodesCount(0), head(nullptr) {}

List::~List()
{
    while (nodesCount != 0)
        pop();
}

void List::insert(string text)
{
    Node* node = new Node(text);
    node->setNext(head);
    nodesCount++;
    head = node;
}

void List::sortInsert(string text)
{
    Node* previous = head;
    if (previous == nullptr)
    {
        this->insert(text);
        return;
    }

    Node* next = previous->getNext();
    while (next != nullptr && next->getData() <= text)
    {
        previous = previous->getNext();
        next = previous->getNext();
    }

    if (previous->getData() == text)
    {
        previous->incrementCounter();
        return;
    }

    Node* node = new Node(text);
    if (previous == nullptr)
    {
        node->setNext(nullptr);
        head = node;
    }

    node->setNext(previous->getNext());
    previous->setNext(node);
    nodesCount++;
}

void List::pop()
{
    if (head == nullptr) return;

    Node* currentHead = head;
    head = head->getNext();
    nodesCount--;
    delete currentHead;
}

void List::print(ostream& out) const
{
    Node* next = head;
    while (next != nullptr)
    {
        out << next->getData() << " (x" << next->getCounter() + 1 << ")" << endl;
        next = next->getNext();
    }
}

int List::getNodesCount() const
{
    return nodesCount;
}

Node* List::getHead() const
{
    return head;
}
