#ifndef LIST_H
#define LIST_H

#include <ostream>
#include <string>

#include "node.h"

using std::cout;
using std::ostream;

class List
{
private:
    int nodesCount;
    Node* head;

public:
    List();
    ~List();

    void insert(string text);
    void sortInsert(string text);
    void pop();
    void print(ostream& out = cout) const;

    int getNodesCount() const;
    Node* getHead() const;
};

#endif /* LIST_H */
