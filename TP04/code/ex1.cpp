#include "list.h"

int main()
{
    List list = List();
    list.sortInsert("a");
    list.sortInsert("b");
    list.sortInsert("a");
    list.sortInsert("e");
    list.sortInsert("c");
    list.sortInsert("c");
    list.sortInsert("d");

    list.print();
    return 0;
}
