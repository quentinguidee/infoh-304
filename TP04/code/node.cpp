#include "node.h"

Node::Node(string text) :
    data(text),
    next(nullptr),
    counter(0)
{
}

void Node::setNext(Node* n)
{
    next = n;
}

Node* Node::getNext() const
{
    return next;
}

string Node::getData() const
{
    return data;
}

int Node::getCounter() const
{
    return counter;
}

void Node::incrementCounter()
{
    counter++;
}
