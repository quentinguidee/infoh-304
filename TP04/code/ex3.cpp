#include <exception>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>

#include "list.h"

using std::ifstream;
using std::ofstream;
using std::string;

List readFile(const string& filename);
void writeFile(const string& filename, const List& list);

int main(int argc, char* argv[])
{
    if (argc != 3)
        return 1;

    const string& inputFilename = argv[1];
    const string& outputFilename = argv[2];

    List list = readFile(inputFilename);
    writeFile(outputFilename, list);

    return 0;
}

List readFile(const string& filename)
{
    ifstream file(filename);
    if (!file.is_open())
        throw std::runtime_error("Impossible d'ouvrir le fichier " + filename);

    List list;
    string word;

    while (file >> word)
        list.sortInsert(word);

    file.close();

    return list;
}

void writeFile(const string& filename, const List& list)
{
    ofstream file(filename);
    if (!file.is_open())
        throw std::runtime_error("Impossible d'ouvrir le fichier " + filename);

    list.print(file);
    file.close();
}
