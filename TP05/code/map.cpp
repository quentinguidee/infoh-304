#include <fstream>
#include <iostream>
#include <map>
#include <string>
#include <utility>

using std::endl;
using std::ifstream;
using std::map;
using std::ofstream;
using std::pair;
using std::string;

typedef map<string, unsigned short> Map;

Map readFile(const string& filename);
void writeFile(const string& filename, Map& words);

int main(int argc, char* argv[])
{
    if (argc != 2)
        return 1;

    const string& inputFilename = argv[1];

    Map words = readFile(inputFilename);
    writeFile("word_count_map.txt", words);

    return 0;
}

Map readFile(const string& filename)
{
    ifstream file(filename);
    if (!file.is_open())
        throw std::runtime_error("Impossible d'ouvrir le fichier " + filename);

    Map words;

    string word;
    while (file >> word)
    {
        pair<Map::iterator, bool> result = words.insert(pair<string, unsigned int>(word, 1));
        if (!result.second) result.first->second++;
    }

    file.close();

    return words;
}

void writeFile(const string& filename, Map& words)
{
    ofstream file(filename);
    if (!file.is_open())
        throw std::runtime_error("Impossible d'ouvrir le fichier " + filename);

    for (Map::iterator it = words.begin(); it != words.end(); it++)
    {
        pair<string, unsigned short> item = *it;
        file << item.first << " (x" << item.second << ")" << endl;
    }

    file.close();
}
