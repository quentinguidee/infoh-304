#include <fstream>
#include <iostream>
#include <set>
#include <string>

using std::cout;
using std::endl;
using std::ifstream;
using std::set;
using std::string;

set<string> readFile(const string& filename);

int main(int argc, char* argv[])
{
    if (argc != 2)
        return 1;

    const string& inputFilename = argv[1];

    set<string> words = readFile(inputFilename);

    cout << "Nombre de mots: " << words.size() << endl;
    return 0;
}

set<string> readFile(const string& filename)
{
    ifstream file(filename);
    if (!file.is_open())
        throw std::runtime_error("Impossible d'ouvrir le fichier " + filename);

    set<string> words;

    string word;
    while (file >> word)
        words.insert(word);

    file.close();

    return words;
}
