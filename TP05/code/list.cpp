#include "../../TP04/code/list.h"

#include <exception>
#include <fstream>
#include <iostream>
#include <stdexcept>
#include <string>

using std::ifstream;
using std::ofstream;
using std::string;

List readFile(const string& filename);

int main(int argc, char* argv[])
{
    if (argc != 2)
        return 1;

    const string& inputFilename = argv[1];

    List list = readFile(inputFilename);

    return 0;
}

List readFile(const string& filename)
{
    ifstream file(filename);
    if (!file.is_open())
        throw std::runtime_error("Impossible d'ouvrir le fichier " + filename);

    List list;
    string word;

    while (file >> word)
        list.sortInsert(word);

    file.close();

    return list;
}
