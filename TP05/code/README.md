# Séance 3

## Compilation

**Nécessite que les fichiers list.cpp et node.cpp soient réalisés dans `TP04/code`**

Tout compiler :

```bash
make
```

Compiler les programmes indépendamment :

```bash
make java
make list
make set
make map
```

Clean :

```bash
make clean
```

## Run

Avec le fichier par défaut :

```bash
make time
```

Avec le fichier sherlock.txt (plus rapide) :

```bash
make time FILE=sherlock.txt
```

Retourne

```plain
time java WordCount sherlock.txt > /dev/null

real    0m0.226s
user    0m0.529s
sys     0m0.043s

time ./map sherlock.txt > /dev/null

real    0m0.465s
user    0m0.218s
sys     0m0.023s

time ./list sherlock.txt > /dev/null

real    0m30.837s
user    0m30.461s
sys     0m0.121s

```
